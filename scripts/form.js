let setup = function() {
    const FORM = document.querySelector("#form")
    const TABLECONT = document.querySelector("#tableHere")
    
    const INPUT = {
        row_count: document.querySelector("#row_count"),
        column_count: document.querySelector("#column_count"),
        odd_color: document.querySelector("#odd_color"),
        even_color: document.querySelector("#even_color"),
        submit: document.querySelector("#submit")
    }

    function createTable(){
        let table = getTable(INPUT.row_count.value, INPUT.column_count.value, INPUT.odd_color.value, INPUT.even_color.value);
        TABLECONT.innerHTML = "";
        TABLECONT.appendChild(table);
    }
    FORM.addEventListener("submit", (e) => {
        e.preventDefault();
        createTable();
    })

}

setup();