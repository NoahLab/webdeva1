function getTable (rows, cols, even, odd) {
    let table = document.createElement("table")
    for(let i = 0; i < rows; i ++){
        let row = document.createElement("tr")
        table.appendChild(row)
        for(let j = 0; j < cols; j ++){
            let td = document.createElement("td");
            td.style.backgroundColor = ((i % 2) ^ (j % 2) ? even : odd)
            td.textContent = `${j},${i}`
            row.appendChild(td);
        }
    }
    return table;
}